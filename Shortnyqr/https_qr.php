<?php
include "phpqrcode.php";
$url = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
$burl = strstr( $url,'qr.php');
$qurl = strstr( strstr( $url, '?url='), 'https');
if ($burl !== "qr.php?" and strpos($burl,'?url=https://') !== false) {
$value=$qurl;
}
else {
$value = "https://www.moerats.com/";
}
$errorCorrectionLevel = "L";
$matrixPointSize = "8";
$margin="1";
QRcode::png($value, false, $errorCorrectionLevel, $matrixPointSize,$margin);
echo $burl;
exit;
?>