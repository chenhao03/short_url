#### 说明

#Nginx，点击域名设置-伪静态，然后将下面例子的域名改成自己的，再复制进去，例子：

rewrite ^/about /about.php last;

rewrite ^/api-about /api-about.php last;

rewrite ^/contact /contact.php last;

rewrite ^/tos /tos.php last;

rewrite ^/([^/.]+)/?$ /link.php?id=$1 last;

rewrite ^/404 /404.php last;

error_page 404 http://www.d163.net/404;

error_page 403 http://www.d163.net/404;


最后开始安装，由于伪静态问题，只能通过具体路径安装，链接如下：



#安装路径，记得修改下面域名地址

https://www.d163.net/install/index.html

#后台路径

https://www.d163.net/admin/index.php

安装完成后，默认用户名和密码均为admin。
